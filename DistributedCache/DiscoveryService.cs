﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;

namespace DistributedCache
{
  /// <summary>
  /// Сервис обнаружения других узлов кэша.
  /// </summary>
  public class DiscoveryService : IDisposable
  {
    #region Поля и свойства

    /// <summary>
    /// Список дескрипторов обнаруженных узлов.
    /// </summary>
    public IEnumerable<PeerDescriptor> DiscoveredPeers => (IReadOnlyCollection<PeerDescriptor>) this.servicesDescriptionsTable.Values;

#if DEBUG

    /// <summary>
    /// Интервал посылки heartbeat-сообщений.
    /// </summary>
    private const int SendHeartbeatInterval = 7;

#else

/// <summary>
/// Интервал посылки heartbeat-сообщений.
/// </summary>
    private const int SendHeartbeatInterval = 30;

#endif

    /// <summary>
    /// Интервал очистки таблицы описаний других сервисов.
    /// </summary>
    private const int PeerDiscoveryCleanUpInterval = SendHeartbeatInterval * 3;

    /// <summary>
    /// Токен для завершения асинхронных операции.
    /// </summary>
    private readonly CancellationTokenSource cts = new CancellationTokenSource();

    /// <summary>
    /// UDP сокет.
    /// </summary>
    private UdpClient udpClient;

    /// <summary>
    /// Таймер, запускающий отправку heartbeat сообщений в сеть. 
    /// </summary>
    private readonly System.Timers.Timer heartBeatActivityTimer = new System.Timers.Timer();

    /// <summary>
    /// 
    /// </summary>
    private readonly System.Timers.Timer descriptionsCleanUpTimer = new System.Timers.Timer();

    /// <summary>
    /// Логгер.
    /// </summary>
    private static readonly ILogger logger = Log.Logger;

    /// <summary>
    /// Признак, указывающий, что сервис уже был запущен.
    /// </summary>
    private volatile bool isAlreadyStarted;

    /// <summary>
    /// Таблица дексрипторов узлов кэша.
    /// </summary>
    private readonly ConcurrentDictionary<string, PeerDescriptor> servicesDescriptionsTable = new ConcurrentDictionary<string, PeerDescriptor>();

    /// <summary>
    /// Конфигурация приложения.
    /// </summary>
    private AppEngine.Options configuration;

    #endregion

    #region Методы

    /// <summary>
    /// Запустить сервис обнаружения.
    /// </summary>
    public void StartDiscovery(AppEngine.Options options)
    {
      if (options == null)
        throw new ArgumentNullException(nameof(options));

      if (this.isAlreadyStarted)
        return;

      this.configuration = options;
      this.OpenUdpClientOnFreePort();
      this.StartToSendingHeartbeatActivity();
      this.StartListenIncomingHeartbeatEvents();
      this.InitializeCleanUpDescriptionsTable();
      logger.Information("The Discovery Service has been started...");
    }

    /// <summary>
    /// Выбрать свободный порт из списка доступных и привязать udp-сокет к этому порту.
    /// </summary>
    private void OpenUdpClientOnFreePort()
    {
      try
      {
        this.isAlreadyStarted = true;
        this.udpClient = new UdpClient
        {
          EnableBroadcast = true,
          ExclusiveAddressUse = false
        };
        this.udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        this.udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, this.configuration.DiscoveryPort));
        logger.Information($"Using heartbeat port: {this.configuration.DiscoveryPort}");

        return;
      }
      catch
      {
        // гасим все исключения.
      }

      throw new IOException("All ports range is busy, please choose another ports range and restart application");
    }

    /// <summary>
    /// Начать периодическую отправку heartbeat-пакетов в сеть.
    /// </summary>
    private void StartToSendingHeartbeatActivity()
    {
      this.SendHearbeat();
      this.heartBeatActivityTimer.Elapsed += (sender, args) =>
      {
        this.SendHearbeat();
        this.heartBeatActivityTimer.Enabled = true;
      };
      this.heartBeatActivityTimer.Interval = TimeSpan.FromSeconds(SendHeartbeatInterval).TotalMilliseconds;
      this.heartBeatActivityTimer.AutoReset = false;
      this.heartBeatActivityTimer.Start();
    }

    /// <summary>
    /// Отправить широковещательоное heartbeat-сообщение.
    /// </summary>
    private async void SendHearbeat()
    {
      try
      {
        var heartbeatMessage = new HeartbeatMessage
        {
          SenderId = this.configuration.InstanceId,
          HttpPort = this.configuration.HttpPort
        };
        var serializedMessage = JsonConvert.SerializeObject(heartbeatMessage);
        var binaryMessage = Encoding.ASCII.GetBytes(serializedMessage);

        using (var udpSender = new UdpClient())
          await udpSender.SendAsync(binaryMessage, binaryMessage.Length, new IPEndPoint(IPAddress.Broadcast, this.configuration.DiscoveryPort));
      }
      catch (Exception ex)
      {
        logger.Error($"Unable to send heartbeat message to port {this.configuration.DiscoveryPort}, occurred exception {ex.Message}");
      }

      logger.Information($"Sended broadcast heartbeat message to port {this.configuration.DiscoveryPort}.");
    }

    /// <summary>
    /// Запустить прослушивание heartbeat сообщений.
    /// </summary>
    private void StartListenIncomingHeartbeatEvents()
    {
      Task.Run(new Action(async () =>
      {
        while (!this.cts.IsCancellationRequested)
        {
          var result = await this.udpClient.ReceiveAsync();
          var serializedMessage = Encoding.ASCII.GetString(result.Buffer);
          var heartbeatMessage = JsonConvert.DeserializeObject<HeartbeatMessage>(serializedMessage);

          if (string.IsNullOrEmpty(heartbeatMessage.SenderId))
            continue;

          // Фильтруем сообщения, в которых текущий процесс является отправителем.
          if (heartbeatMessage.SenderId == this.configuration.InstanceId)
            continue;

          var senderIp = result.RemoteEndPoint.Address.ToString();
          logger.Information($"Receive hearbeat message from {senderIp}, id={heartbeatMessage.SenderId}");
          this.servicesDescriptionsTable.AddOrUpdate(heartbeatMessage.SenderId, s =>
          {
            var peer = new PeerDescriptor
            {
              Id = heartbeatMessage.SenderId,
              Port = heartbeatMessage.HttpPort,
              LastActivity = DateTime.UtcNow,
              Ip = senderIp
            };

            Log.Logger.Information($"Discovered new peer {peer.Ip}:{peer.Port}:{peer.Id}");

            return peer;
          }, (s, description) =>
          {
            description.LastActivity = DateTime.UtcNow;

            return description;
          });
        }
      }));
    }

    /// <summary>
    /// Запустить периодическую очистку таблицы сведений о других узлах кэша.
    /// </summary>
    private void InitializeCleanUpDescriptionsTable()
    {
      this.descriptionsCleanUpTimer.Elapsed += (sender, args) => this.CleanUpDescriptionsTable();
      this.descriptionsCleanUpTimer.Interval = TimeSpan.FromSeconds(PeerDiscoveryCleanUpInterval).TotalMilliseconds;
      this.descriptionsCleanUpTimer.Start();
    }

    /// <summary>
    /// Очистить таблицу сведений о других узлах кэша.
    /// </summary>
    private void CleanUpDescriptionsTable()
    {
      var thresholdDate = DateTime.UtcNow.AddSeconds(SendHeartbeatInterval * (-1));
      var obosletePeers = this.servicesDescriptionsTable.Values.Where(s => s.LastActivity < thresholdDate);
      foreach (var peer in obosletePeers)
      {
        if (this.servicesDescriptionsTable.TryRemove(peer.Id, out var p))
          logger.Information($"Deleted obsolete peer {p.Ip}:{p.Port}:{p.Id}");
      }
    }

    #endregion

    #region IDisposable

    public void Dispose()
    {
      this.udpClient.Dispose();
    }

    #endregion

    #region Вложенные типы

    /// <summary>
    /// POCO класс для heartbeat сообщения.
    /// </summary>
    private class HeartbeatMessage
    {
      /// <summary>
      /// Идентификатор узла, пославшего сообщение.
      /// </summary>
      public string SenderId { get; set; }

      /// <summary>
      /// HTTP порт узла, на котором принимаются входящие http подключения.
      /// </summary>
      public int HttpPort { get; set; }
    }

    /// <summary>
    /// Описание соседнего узла кэша.
    /// </summary>
    public class PeerDescriptor
    {
      /// <summary>
      /// Время последней активности узла.
      /// </summary>
      public DateTime LastActivity { get; set; }

      /// <summary>
      /// IP узла.
      /// </summary>
      public string Ip { get; set; }

      /// <summary>
      /// Идентификатор узла.
      /// </summary>
      public string Id { get; set; }

      /// <summary>
      /// Порт, на котором узел принимает http запросы.
      /// </summary>
      public int Port { get; set; }
    }

    #endregion
  }
}