﻿using System;
using DistributedCache.Web;
using Microsoft.Owin.Hosting;
using Serilog;
using Unity;

namespace DistributedCache
{
  /// <summary>
  /// Приложение.
  /// </summary>
  public class AppEngine : IDisposable
  {
    /// <summary>
    /// Служба обнаружения других инстансов.
    /// </summary>
    private DiscoveryService discoveryService;

    /// <summary>
    /// Веб приложение.
    /// </summary>
    private IDisposable webApplication;

    /// <summary>
    /// Запустить приложение.
    /// </summary>
    /// <param name="options">Настройки приложения.</param>
    public void Start(Options options)
    {
      if (options == null)
        throw new ArgumentNullException(nameof(options));

      this.ConfigureLogger();
      this.ConfigureAndRunDiscoveryService(options);
      this.ConfigureDependencyContainer();
      this.webApplication = WebApp.Start<Startup>($"http://*:{options.HttpPort}/");
      Log.Logger.Information($"Application started, http port {options.HttpPort}.");
    }

    /// <summary>
    /// Сконфигурировать и запустить службу обнаружения других узлов кэша.
    /// </summary>
    /// <param name="options">Аргументы запуска.</param>
    private void ConfigureAndRunDiscoveryService(Options options)
    {
      this.discoveryService = new DiscoveryService();
      this.discoveryService.StartDiscovery(options);
    }

    /// <summary>
    /// Сконфигурировать логгер.
    /// </summary>
    private void ConfigureLogger()
    {
      var config = new LoggerConfiguration()
        .WriteTo.Console()
        .WriteTo.RollingFile("log.txt")
        .MinimumLevel.Debug();

      Log.Logger = config.CreateLogger();
    }

    /// <summary>
    /// Собрать контейнер зависимостей.
    /// </summary>
    private void ConfigureDependencyContainer()
    {
      var unityContainer = new UnityContainer();
      unityContainer.RegisterType<CacheService>();
      unityContainer.RegisterType<PeerProxy>();
      unityContainer.RegisterInstance(this.discoveryService);
      Startup.UnityContainer = unityContainer;
    }

    #region IDisposable

    public void Dispose()
    {
      this.discoveryService?.Dispose();
      this.webApplication?.Dispose();
    }

    #endregion

    /// <summary>
    /// Настройки приложения.
    /// </summary>
    public class Options
    {
      /// <summary>
      /// Порт, который используется для прослушивания/отправки heartbeat сигналов.
      /// </summary>
      public ushort DiscoveryPort { get; set; }

      /// <summary>
      /// Порт для обработки входящих запросов.
      /// </summary>
      public ushort HttpPort { get; set; }

      /// <summary>
      /// Идентификатор текущего узла.
      /// </summary>
      public string InstanceId { get; set; }
    }
  }
}