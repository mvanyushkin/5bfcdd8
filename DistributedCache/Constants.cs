﻿namespace DistributedCache
{
  /// <summary>
  /// Константы.
  /// </summary>
  public static class Constants
  {
    /// <summary>
    /// Кастомный HTTP заголовок, для определния запроса от инстанса
    /// распределенного кэша, чтобы не было рекурсивных вызовов от узла к узлу.
    /// </summary>
    public const string ForwardedFromHeader = "X-ForwardedFromPeer";
    
    /// <summary>
    /// Имя службы.
    /// </summary>
    public const string ServiceName = "Distributed Cache";
  }
}