﻿using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Serilog;

namespace DistributedCache
{
  /// <summary>
  /// Сервис кэша.
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public sealed class CacheService
  {
    /// <summary>
    /// Прокси для доступа к другим узлам кэша.
    /// </summary>
    private readonly PeerProxy peerProxy;

    /// <summary>
    /// Реальное хранилище данных.
    /// </summary>
    private static readonly ConcurrentDictionary<string, CacheItem> internalCache = new ConcurrentDictionary<string, CacheItem>();
    
    #region Методы

    /// <summary>
    /// Установить значение в кэше.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    /// <param name="contentType">Тип содержимого.</param>
    /// <param name="value">Содержимое.</param>
    public void SetValue(string key, string contentType, byte[] value) =>
      internalCache.AddOrUpdate(key, s => new CacheItem { Data = value, ContentType = contentType }, (cacheItemKey, o) => o);

    /// <summary>
    /// Удалить значение из кэша.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    /// <param name="executeOnlyHere">Выполнить действие только на этом экземпляре кэша.</param>
    public async Task DeleteValueAsync(string key, bool executeOnlyHere)
    { 
      // ReSharper disable once UnusedVariable
      if (!internalCache.TryRemove(key, out var value))
      {
        if (executeOnlyHere)
        {
          Log.Logger.Debug($"Unable to find value by key '{key}' in local storage and stop propagate delete event");
          return;
        }
        
        Log.Logger.Debug($"Unable to find value by key '{key}' in local storage, proxying request to other nodes...");
        await this.peerProxy.DeleteOnOtherPeers(key);
      }
    }

    /// <summary>
    /// Получить значение по ключу.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    /// <param name="executeOnlyHere">Выполнить действие только на этом экземпляре кэша.</param>
    /// <returns>Структура вида "поток содержимого:тип содержимого"</returns>
    public async Task<(Stream, string)> GetValueAsync(string key, bool executeOnlyHere)
    { 
      if (internalCache.TryGetValue(key, out var value))
        return (new MemoryStream(value.Data), value.ContentType);

      if (executeOnlyHere)
      {
        Log.Logger.Debug($"Unable to find value by key '{key}' in local storage and stop propagate delete event");
        return (null, null);
      }

      Log.Logger.Debug($"Unable to find value by key '{key}' in local storage, proxying request to other nodes...");
      return await this.peerProxy.GetValueFromOtherPeersAsync(key);
    }

    #endregion

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="peerProxy">Прокси для доступа к другим узлам кэша.</param>
    public CacheService(PeerProxy peerProxy)
    {
      this.peerProxy = peerProxy;
    }

    #region Вложенный типы

    /// <summary>
    /// Элемент кэша.
    /// </summary>
    private struct CacheItem
    {
      /// <summary>
      /// Данные.
      /// </summary>
      public byte[] Data { get; set; }
      
      /// <summary>
      /// Тип данных.
      /// </summary>
      public string ContentType { get; set; }
    }

    #endregion
  }
}