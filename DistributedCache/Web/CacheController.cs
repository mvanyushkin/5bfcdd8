﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Serilog;

namespace DistributedCache.Web
{
  /// <summary>
  /// Http-апи для доступа к кэшу.
  /// </summary>
  public class CacheController : ApiController
  {
    /// <summary>
    /// Сам кэш.
    /// </summary>
    private readonly CacheService cacheService;

    /// <summary>
    /// Получить запись из кэша.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    [HttpGet]
    [Route("data/{key}")]
    public async Task<object> Get(string key)
    {
      Log.Logger.Debug($"Getting value by key '{key}'.");
      if (string.IsNullOrEmpty(key))
        throw new ArgumentNullException(nameof(key));

      var stopPropagation = this.Request.Headers.Contains(Constants.ForwardedFromHeader);
      var value = await this.cacheService.GetValueAsync(key, stopPropagation);
      if (value.Item1 == null)
        return this.NotFound();
      
      var responseMessage = new HttpResponseMessage { Content = new StreamContent(value.Item1) };
      responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(value.Item2);
      responseMessage.Content.Headers.ContentLength = value.Item1.Length;
      // Чтобы не было утечек, нужно задиспозить поток,
      // в частности тут же регистрируется на диспоз HttPeerProxyStream,
      // чтобы закрылся связанный с ним HttpClient.
      this.Request.RegisterForDispose(value.Item1);
      return responseMessage;
    }
    
    /// <summary>
    /// Получить запись из кэша.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    [HttpPost]
    [Route("data/{key}")]
    public async Task<IHttpActionResult> Post(string key)
    {
      Log.Logger.Debug($"Posting value by key '{key}'.");
      if (string.IsNullOrEmpty(key))
        throw new ArgumentNullException(nameof(key));

      var value = await this.Request.Content.ReadAsByteArrayAsync();
      this.cacheService.SetValue(key, this.Request.Content.Headers.ContentType.ToString(), value);
      return this.Ok();
    }
    
    /// <summary>
    /// Получить запись из кэша.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    [HttpDelete]
    [Route("data/{key}")]
    public async Task<IHttpActionResult> Delete(string key)
    {
      Log.Logger.Debug($"Deleting value by key '{key}'.");
      if (string.IsNullOrEmpty(key))
        throw new ArgumentNullException(nameof(key));
      
      
      var stopPropagation = this.Request.Headers.Contains("X-ForwardedFromPeer");
      await this.cacheService.DeleteValueAsync(key, stopPropagation);
      return this.Ok();
    }

    /// <summary>
    /// конструктор.
    /// </summary>
    public CacheController(CacheService cacheService)
    {
      this.cacheService = cacheService;
    }
  }
}