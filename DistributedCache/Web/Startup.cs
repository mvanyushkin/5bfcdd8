﻿using System.Web.Http;
using DistributedCache.Web;
using Microsoft.Owin;
using Owin;
using Unity;
using Unity.WebApi;

[assembly: OwinStartup(typeof(Startup))]

namespace DistributedCache.Web
{
  /// <summary>
  /// Точка входа в веб апи.
  /// </summary>
  public class Startup
  {
    /// <summary>
    /// Контейнер зависимостей.
    /// </summary>
    public static UnityContainer UnityContainer { private get; set; }
    
    /// <summary>
    /// Конфигурирование веб апи.
    /// </summary>
    /// <param name="app">Билдер веб апи приложения.</param>
    // ReSharper disable once UnusedMember.Global
    public void Configuration(IAppBuilder app)
    {
      var config = new HttpConfiguration();
      config.MapHttpAttributeRoutes();
      config.DependencyResolver = new UnityDependencyResolver(UnityContainer);
      app.UseWebApi(config);
    }
  }
}
