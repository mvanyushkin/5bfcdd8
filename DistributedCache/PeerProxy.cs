﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace DistributedCache
{
  /// <summary>
  /// Прокси для работы с другими узлами сети.
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public class PeerProxy
  {
    /// <summary>
    /// Сервис обнаружения.
    /// </summary>
    private readonly DiscoveryService discoveryService;

    /// <summary>
    /// Получить значение от других узлов кэша.
    /// </summary>
    /// <param name="key">Ключ.</param>
    /// <returns>Структура вида "поток содержимого:тип содержимого"</returns>
    public async Task<(Stream, string)> GetValueFromOtherPeersAsync(string key)
    {
      if (string.IsNullOrEmpty(key))
        throw new ArgumentNullException(nameof(key));

      var taskCompletionSource = new TaskCompletionSource<(Stream, string)>();
      var cancellationTokenSource = new CancellationTokenSource();
      if (!this.discoveryService.DiscoveredPeers.Any())
        return (null, null);


      var parallelOptions = new ParallelOptions
      {
        // Здесь только io-bound, поэтому берем значение побольше.
        MaxDegreeOfParallelism = 100
      };

      int failedRequests = 0;
      var discoveredPeers = this.discoveryService.DiscoveredPeers;
      Parallel.ForEach(discoveredPeers, parallelOptions, async (peer, state) =>
      {
        // 1) запускаем все запросы параллельно
        // 2) первый выполненный таск, респонс которого имеет правильный статус код, отменяет все остальные вызовы
        // 3) и результат этого первого правильного выполненного таски возвращаем вызывающему коду
        // Такое усложнение введено для ускорения получения записи.
        var client = new HttpClient(); 
        try
        {
          client.Timeout = TimeSpan.FromSeconds(5);
          client.DefaultRequestHeaders.Add(Constants.ForwardedFromHeader, string.Empty);
          Log.Logger.Debug($"Asking peer {peer.Ip}:{peer.Port}:{peer.Id}...");
          var responseMessage = await client.GetAsync($"http://{peer.Ip}:{peer.Port}/data/{key}", cancellationTokenSource.Token);
          responseMessage.EnsureSuccessStatusCode();
          cancellationTokenSource.Cancel();
          Log.Logger.Debug($"Peer {peer.Ip}:{peer:Port}:{peer.Id} returned value by key {key}");
          taskCompletionSource.SetResult((new HttpPeerProxyStream(client, responseMessage), responseMessage.Content.Headers.ContentType.ToString()));
        }
        catch
        {
          // Если была ошибка или запись не нашлась по запросу, то нужно уничтожать прямо здесь.
          client.Dispose();
          Interlocked.Increment(ref failedRequests);
          // Если ни один из запросов не завершился удачно, то возвращаем явно Null.
          if (Volatile.Read(ref failedRequests) == discoveredPeers.Count())
          {
            taskCompletionSource.SetResult((null, null));
          }
        }
      });
   
      return await taskCompletionSource.Task;
    }

    /// <summary>
    /// Стереть запись на других узлах кэша.
    /// </summary>
    /// <param name="key">Ключ записи.</param>
    public async Task DeleteOnOtherPeers(string key)
    {
      var tasks = this.discoveryService.DiscoveredPeers.Select(peer =>
      {
        return Task.Run(async () =>
        {
          using (var client = new HttpClient())
          {
            client.Timeout = TimeSpan.FromSeconds(5);
            Log.Logger.Debug($"Deleting value on peer {peer.Ip}:{peer.Port}:{peer.Id}...");
            client.DefaultRequestHeaders.Add(Constants.ForwardedFromHeader, string.Empty);
            await client.DeleteAsync($"http://{peer.Ip}:{peer.Port}/data/{key}");
          }
        });
      });

      await Task.WhenAll(tasks.ToArray());
    }
    
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="discoveryService">Сервис обнаружения других узлов кэша.</param>
    public PeerProxy(DiscoveryService discoveryService)
    {
      this.discoveryService = discoveryService;
    }
  }
}