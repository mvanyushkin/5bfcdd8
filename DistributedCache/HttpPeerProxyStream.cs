﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DistributedCache
{
  /// <summary>
  /// Класс для потоковой передачи данных от другого узла кэша к клиенту.
  /// </summary>
  public class HttpPeerProxyStream : Stream
  {
    /// <summary>
    /// Http-клиент, качающий данные с другого узла.
    /// </summary>
    private readonly HttpClient client;

    /// <summary>
    /// Ответ от узла, c которого можно качать данные.
    /// </summary>
    private readonly HttpResponseMessage response;

    /// <summary>
    /// Поток данных.
    /// </summary>
    private readonly Stream valueStream;

    #region Базовый класс

    public override void Flush() { }

    public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

    public override void SetLength(long value) => this.valueStream.SetLength(value);

    public override int Read(byte[] buffer, int offset, int count) => this.valueStream.Read(buffer, offset, count);

    public override void Write(byte[] buffer, int offset, int count) => this.valueStream.Write(buffer, offset, count);

    public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken) => this.valueStream.ReadAsync(buffer, offset, count, cancellationToken);

    public override bool CanRead => this.valueStream.CanRead;

    public override bool CanSeek => this.valueStream.CanSeek;

    public override bool CanWrite => this.valueStream.CanWrite;

    public override long Length => this.valueStream.Length;

    public override long Position
    {
      get => this.valueStream.Position;
      set => this.valueStream.Position = value;
    }

    #endregion

    #region IDisposable

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.valueStream.Dispose();
        this.response.Dispose();
        this.client.Dispose();  
      }
      base.Dispose(disposing);
    }

    #endregion

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="client">HTTP-клиент.</param>
    /// <param name="response">Объект http-ответа.</param>
    public HttpPeerProxyStream(HttpClient client, HttpResponseMessage response)
    {
      this.client = client;
      this.response = response;
      this.valueStream = response.Content.ReadAsStreamAsync().Result;
    }

    /// <summary>
    /// Статический конструктор.
    /// </summary>
    static HttpPeerProxyStream()
    {
      // Сразу увеличиваем количество конкуретных http запросов.
      ServicePointManager.DefaultConnectionLimit = 10000;
    }
  }
}