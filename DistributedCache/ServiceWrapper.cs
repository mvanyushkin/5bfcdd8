﻿using System.ServiceProcess;

namespace DistributedCache
{
  /// <summary>
  /// Обертка для запуска приложения в режиме службы Windows.
  /// </summary>
  public class ServiceWrapper : ServiceBase
  {
    /// <summary>
    /// Экземпляр приложения.
    /// </summary>
    private readonly AppEngine appEngine;

    #region Базовый класс

    protected override void OnStart(string[] args)
    {
      var options = EntryPoint.GetAppOptions();
      this.appEngine.Start(options);
    }

    protected override void OnStop()
    {
      this.appEngine.Dispose();
    }

    #endregion
 
    /// <summary>
    /// Конструктор.
    /// </summary>
    public ServiceWrapper()
    {
      this.ServiceName = Constants.ServiceName;
      this.appEngine = new AppEngine();
    }
  }
}