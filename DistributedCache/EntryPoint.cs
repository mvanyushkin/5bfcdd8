﻿using System;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

namespace DistributedCache
{
  /// <summary>
  /// Точка входа в приложение.
  /// </summary>
  internal static class EntryPoint
  {
    /// <summary>
    /// Точка входа в приложение.
    /// </summary>
    /// <param name="args">Аргументы командной строки.</param>
    public static void Main(string[] args)
    {
      if (args.Any(arg => arg == "--console"))
      {
        using (var appEngine = new AppEngine())
        {
          var options = GetAppOptions();
          appEngine.Start(options);
          // Console.ReadKey() не подошел,
          // где-то дедлок на доступе к консоли в логгере, не стал разбираться.
          var are = new AutoResetEvent(false);
          are.WaitOne();
        }
        return;
      }
       
      ServiceBase.Run(new ServiceWrapper());
    }

    /// <summary>
    /// Получить настройки приложения.
    /// </summary>
    /// <returns>Настройки приложения.</returns>
    public static AppEngine.Options GetAppOptions()
    {
      var httpPortFromConfig = ConfigurationManager.AppSettings["HttpPort"];
      if (!ushort.TryParse(httpPortFromConfig, out var httpPort))
        httpPort = 9000;

      var udpPortFromConfig = ConfigurationManager.AppSettings["DiscoveryPort"];
      if (!ushort.TryParse(udpPortFromConfig, out var udpPort))
        udpPort = 10000;

      var options = new AppEngine.Options
      {
        DiscoveryPort = udpPort,
        HttpPort = httpPort,
        InstanceId = Guid.NewGuid().ToString()
      };

      return options;
    }
  }
}